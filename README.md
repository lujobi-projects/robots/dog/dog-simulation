[![Imports: isort](https://img.shields.io/badge/%20imports-isort-%231674b1?style=flat&labelColor=ef8336)](https://pycqa.github.io/isort/)
# Test

## Setup

### Windows

more explaination [here](https://medium.com/@potatowagon/how-to-use-gui-apps-in-linux-docker-container-from-windows-host-485d3e1c64a3)

1. [install chocolatey](https://chocolatey.org/install)
2. install XServer for Windows `choco install vcxsrv`
3. Disable access control (Todo)

## Running

``` bash
docker build -t python_test .
docker run -ti --rm -e DISPLAY=YOUR_IP:0.0 python_test
```
