from .cylinder import Cylinder
# from .Sphere import Sphere
from .Torus import Torus
from .Icosphere import Icosphere