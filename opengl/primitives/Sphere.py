import pyglet
from pyglet.gl import *
import numpy as np

def normalize(x):
    return x / np.linalg.norm(x)

import pyglet
from pyglet.gl import *
import numpy as np

from .utils import quad_as_tri


class Cylinder:
    def __init__(self, radius, slices, batch, color, group=None):
        
        self.vertices = ()
        assert slices >= 3

        angles_phi = np.linspace(0, 2*np.pi, slices+1)[:-1]
        angles_theta = np.linspace(-.5*np.pi, .5*np.pi, slices//2)
        
        x = np.cos(angles)
        y = np.sin(angles)
        z = np.ones_like(y)

        self.verts = []
        tmp = np.stack((radius*x, radius*y, 0*z), axis=1)
        tmp2 = np.stack((upper_radius*x, upper_radius*y, height*z), axis=1)

        self.verts.extend(tmp.reshape((1,-1))[0])
        self.verts.extend(tmp2.reshape((1,-1))[0])
        self.verts.extend([0, 0, 0, 0, 0, height])


        self.tris = []
        for i in range(slices):
            self.tris.extend([slices*2, (i+1)%slices, i, slices*2+1, slices + i, slices + (i+1)%slices])
            self.tris.extend(quad_as_tri(i, (i+1)%slices, slices + (i+1)%slices, slices + i))

        for i in range(len(self.tris)//3):
            print(self.tris[3*i:3*i+3])

        verts_len = len(self.verts)//3
        batch.add_indexed(verts_len, GL_TRIANGLES, group, self.tris, ('v3f/static', self.verts), ('c3B/static', color*verts_len))



# # class Sphere(object):
#     vv = []            # vertex vectors
#     vcount = 0
#     vertices = []
#     normals = []
#     textureuvw = []
#     tangents = []
#     indices  = []

#     def myindex( self, list, value ):
#         for idx, obj in enumerate(list):
#             if np.linalg.norm(obj-value) < 0.0001:
#               return idx
#         raise ValueError # not found

#     def splitTriangle(self, i1, i2, i3, new):
#         '''
#         Interpolates and Normalizes 3 Vectors p1, p2, p3.
#         Result is an Array of 4 Triangles
#         '''
#         p12 = self.vv[i1] + self.vv[i2]
#         p23 = self.vv[i2] + self.vv[i3]
#         p31 = self.vv[i3] + self.vv[i1]
#         p12 = normalize(p12)
#         try:
#             if new[0] == "X":
#                 ii0 = self.myindex(self.vv, p12)
#             else:
#                 self.vv.append( p12 )
#                 ii0 = self.vcount
#                 self.vcount += 1
#         except ValueError:
#             print("This should not happen 1")
#         p23 = normalize(p23)
#         try:
#             if new[1] == "X":
#                 ii1 = self.myindex(self.vv, p23)
#             else:
#                 self.vv.append( p23 )
#                 ii1 = self.vcount
#                 self.vcount += 1
#         except ValueError:
#             print("This should not happen 2")
#         p31 = normalize(p31)
#         try:
#             if new[2] == "X":
#                 ii2 = self.myindex(self.vv, p31)
#             else:
#                 self.vv.append( p31 )
#                 ii2 = self.vcount
#                 self.vcount += 1
#         except ValueError:
#             print("This should not happen 3")
#         rslt = []
#         rslt.append([i1,  ii0, ii2])
#         rslt.append([ii0, i2,  ii1])
#         rslt.append([ii0, ii1, ii2])
#         rslt.append([ii2, ii1,  i3])
#         return rslt

#     def recurseTriangle(self, i1, i2, i3, level, new):
#         if level > 0:                     # split in 4 triangles
#             p1, p2, p3, p4 = self.splitTriangle( i1, i2, i3, new )
#             self.recurseTriangle( *p1, level=level-1, new=new[0]+"N"+new[2] )
#             self.recurseTriangle( *p2, level=level-1, new=new[0]+new[1]+"N" )
#             self.recurseTriangle( *p3, level=level-1, new="XNX" )
#             self.recurseTriangle( *p4, level=level-1, new="X"+new[1]+new[2] )
#         else:
#            self.indices.extend( [i1, i2, i3] ) # just MAKE the triangle

#     def flatten(self, x):
#         result = []

#         for el in x:
#             #if isinstance(el, (list, tuple)):
#             if hasattr(el, "__iter__") and not isinstance(el, str):
#                 result.extend(self.flatten(el))
#             else:
#                 result.append(el)
#         return result

#     def __init__(self, radius, slices, batch, group=None):
#         print("Creating Sphere... please wait")
#         # Create the vertex array.
#         self.vv.append( np.array([1.0, 0.0, 0.0]) ) # North
#         self.vv.append( np.array([-1.0, 0.0, 0.0]) ) # South
#         self.vv.append( np.array([ 0.0, 1.0, 0.0]) ) # A
#         self.vv.append( np.array([ 0.0, 0.0, 1.0]) ) # B
#         self.vv.append( np.array([ 0.0,-1.0, 0.0]) ) # C
#         self.vv.append( np.array([ 0.0, 0.0,-1.0]) ) # D
#         self.vcount = 6

#         self.recurseTriangle( 0, 2, 3, slices, "NNN" ) # N=new edge, X=already done
#         self.recurseTriangle( 0, 3, 4, slices, "XNN" )
#         self.recurseTriangle( 0, 4, 5, slices, "XNN" )
#         self.recurseTriangle( 0, 5, 2, slices, "XNX" )
#         self.recurseTriangle( 1, 3, 2, slices, "NXN" )
#         self.recurseTriangle( 1, 4, 3, slices, "NXX" )
#         self.recurseTriangle( 1, 5, 4, slices, "NXX" )
#         self.recurseTriangle( 1, 2, 5, slices, "XXX" )

#         print ("Sphere Level ", slices, " with ", self.vcount, " Vertices")

#         for v in range(self.vcount):
#             self.normals.extend(self.vv[v][:])
#         self.vv = [(x * radius) for x in self.vv]
#         self.vertices = [x[:] for x in self.vv]
#         self.vertices = self.flatten( self.vertices )

#         self.vertex_list = batch.add_indexed(len(self.vertices)//3,
#                                              GL_TRIANGLES,
#                                              group,
#                                              self.indices,
#                                              ('v3f/static', self.vertices),
#                                              ('n3f/static', self.normals))

#     def delete(self):
#         self.vertex_list.delete()