import pyglet
from pyglet.gl import *
import numpy as np

from .utils import quad_as_tri


class Cylinder:
    def __init__(self, radius, height, slices, batch, color, edge_color=None, group=None, upper_radius=None):
        if not upper_radius:
            upper_radius = radius
        
        self.vertices = ()
        assert slices >= 3

        angles = np.linspace(0, 2*np.pi, slices+1)[:-1]
        x = np.cos(angles)
        y = np.sin(angles)
        z = np.ones_like(y)

        self.verts = []
        tmp = np.stack((radius*x, radius*y, 0*z), axis=1)
        tmp2 = np.stack((upper_radius*x, upper_radius*y, height*z), axis=1)

        self.verts.extend(tmp.reshape((1,-1))[0])
        self.verts.extend(tmp2.reshape((1,-1))[0])
        self.verts.extend([0, 0, 0, 0, 0, height])


        self.tris = []
        self.edges = []
        for i in range(slices):
            self.tris.extend([slices*2, (i+1)%slices, i, slices*2+1, slices + i, slices + (i+1)%slices])
            self.tris.extend(quad_as_tri(i, (i+1)%slices, slices + (i+1)%slices, slices + i))
            self.edges.extend([slices*2, i, slices*2+1, i+slices, i, (i+1)%slices, i + slices, (i+1)%slices + slices])
            self.edges.extend([i, slices + i, (i+1)%slices, slices + i])

        verts_len = len(self.verts)//3
        batch.add_indexed(verts_len, GL_TRIANGLES, group, self.tris, ('v3f/static', self.verts), ('c3B/static', color*verts_len))
        if edge_color:    
            batch.add_indexed(verts_len, GL_LINES, group, self.edges, ('v3f/static', self.verts), ('c3B/static', edge_color*verts_len))
