import pyglet
from pyglet.gl import *
import numpy as np

class Icosphere:

    def __init__(self, radius, level, batch, color, edge_color=None, group=None):
        self.radius = radius
        self.vertices = []
        self.triangles = []
        assert level >= 0

        self.vertices, self.triangles, self.edges = self.init_isoc()

        self.vertices = self.vertices.reshape((1,-1))[0]
        self.triangles = np.array(self.triangles).reshape((1,-1))[0]
        self.edges = np.array(self.edges).reshape((1,-1))[0]
        

        verts_len = len(self.vertices)//3
        batch.add_indexed(verts_len, GL_TRIANGLES, group, self.triangles, ('v3f/static', self.vertices), ('c3B/static', color*verts_len))
        if edge_color:    
            batch.add_indexed(verts_len, GL_LINES, group, self.edges, ('v3f/static', self.vertices), ('c3B/static', edge_color*verts_len))

    def init_isoc(self):
        angles = np.linspace(0, 2*np.pi, 6)[:-1]
        rad_offs = self.radius*np.cos(np.arctan(.5))

        x_upper = rad_offs*np.cos(angles)
        y_upper = rad_offs*np.sin(angles)
        z_upper = self.radius*np.arctan(.5)*np.ones_like(x_upper)
        x_lower = rad_offs*np.cos(angles+np.pi/5)
        y_lower = rad_offs*np.sin(angles+np.pi/5)
        z_lower = -z_upper

        tmp_upper = np.stack((x_upper, y_upper, z_upper), axis=1)
        tmp_lower = np.stack((x_lower, y_lower, z_lower), axis=1)
        vertices = np.vstack((tmp_upper, tmp_lower, [0, 0, self.radius], [0, 0, -self.radius]))
        
        triangles = []
        edges = []

        for i in range(5):
            triangles.append([10, i, (i+1)%5])
            triangles.append([11, (i+1)%5+5, i+5])
            triangles.append([i, i+5, (i+1)%5])
            triangles.append([i+5, (i+1)%5+5, (i+1)%5])
            edges.append([10, i])
            edges.append([11, i+5])
            edges.append([i, (i+1)%5])
            edges.append([i+5, (i+1)%5+5])
            edges.append([i, i+5])
            edges.append([(i+1)%5, i+5])
            
        print(edges)
        return vertices, triangles, edges

    def subdivide_triangles(self):
        pass
