import logging
import os.path
import pickle

import numpy as np
from sympy import Matrix, latex, simplify, symbols

from utils.cylinder import Cylinder
from utils.line import draw_coord_sys, plot_line, plot_text
from utils.transformation import (rot, rot_trans, sym_rot_x, sym_rot_y,
                                  sym_rot_z, transform, translate)

JACOBIAN_FILE = 'dog_jac.pickle'

INITIAL_COORDS = np.array([
    0,  # 0:  x
    0,  # 1:  y
    0,  # 2:  z
    0,  # 3:  theta0
    0,  # 4:  theta1
    0,  # 5:  theta2
    0,  # 6:  phi0
    np.pi/4,  # 7:  phi1
    np.pi/4,  # 8:  phi2
    0,  # 9:  phi3
    np.pi/4,  # 10: phi4
    np.pi/4,  # 11: phi5
    0,  # 12: phi6
    np.pi/4,  # 13: phi7
    np.pi/4,  # 14: phi8
    0,  # 15: phi9
    np.pi/4,  # 16: phi10
    np.pi/4,  # 17: phi11
])


class DogMeasures:

    def __init__(self) -> None:
        self.base_back_front = 2
        self.base_back_rear = 2
        self.back_shoulder = 2
        self.shoulder_offset = 1
        self.upper_arm = 2
        self.underarm = 2

    def __eq__(self, o: object) -> bool:
        return  \
        self.base_back_front == o.base_back_front and \
        self.base_back_rear == o.base_back_rear and \
        self.back_shoulder == o.back_shoulder and \
        self.shoulder_offset == o.shoulder_offset and \
        self.upper_arm == o.upper_arm and \
        self.underarm == o.underarm


class Dog:

    def __init__(self, measures=DogMeasures(), calc_jac=False):
        self.measures = measures
        self.coords = symbols('x y z t1 t2 t3 p1 p2 p3 p4 p5 p6 p7 p8 p9 p10 p11 p12')
        self._T_base_back_front = translate(measures.base_back_front, 0, 0)
        self._T_base_back_rear = translate(-measures.base_back_rear, 0, 0)
        self._T_back_shoulder_up_right = np.array([[0, 0, 1, 0], [0, 1, 0, measures.back_shoulder], [-1, 0, 0, 0],
                                                   [0, 0, 0, 1]])
        self._T_back_shoulder_up_left = np.array([[0, 0, 1, 0], [0, 1, 0, -measures.back_shoulder], [-1, 0, 0, 0],
                                                  [0, 0, 0, 1]])
        self._T_shoulder_up_shoulder_low = np.array([[0, 1, 0, measures.shoulder_offset], [0, 0, -1, 0], [-1, 0, 0, 0],
                                                     [0, 0, 0, 1]])
        self._T_upper_lower = np.array([[0, -1, 0, measures.upper_arm], [1, 0, 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
        self._T_elbow_endeffector = translate(measures.underarm, 0, 0)
        self.calc_jac = calc_jac

        if self.calc_jac:
            self.prep_jac()
        self.update()

    def prep_jac(self):
        found = False
        if os.path.isfile(JACOBIAN_FILE):
            with open(JACOBIAN_FILE, 'rb') as file:
                data = pickle.load(file)
            if data.get('measures') == self.measures:
                logging.info('Loading Jacobians form pickle file')
                found = True
                self.J_wb = data.get('J_wb')
                self.J_we_sym = data.get('J_we_sym')
            else:
                logging.info('detected change in measurements, recalculating')
        else:
            logging.info('no jacobian picklefile found, recalculating')

        if not found:
            self.J_wb = np.zeros((6, 18))
            self.J_wb[0, 0] = 1
            self.J_wb[1, 1] = 1
            self.J_wb[2, 2] = 1
            self.J_wb[3, 3] = 1
            self.J_wb[4, 4] = 1
            self.J_wb[5, 5] = 1

            [x, y, z, theta1, theta2, theta3] = self.coords[:6]

            T_wb_sym = translate(x, y, z) * (sym_rot_z(theta3) * (sym_rot_y(theta2) * sym_rot_x(theta1)))
            T_bf_sym = self._T_base_back_front * T_wb_sym
            T_br_sym = self._T_base_back_rear * T_wb_sym
            self.J_we_sym = []

            for i in range(4):
                logging.info('Calculating symbolic Transformation Matrix for Leg %i', i + 1)
                [phi_1, phi_2, phi_3] = self.coords[6 + i * 3:9 + i * 3]
                T_t1 = (self._T_back_shoulder_up_right if i %
                        2 == 0 else self._T_back_shoulder_up_left) * sym_rot_z(phi_1)
                T_b1 = (T_bf_sym if i < 2 else T_br_sym) * T_t1
                T_12 = self._T_shoulder_up_shoulder_low * sym_rot_z(phi_2)
                T_b2 = T_b1 * T_12
                T_23 = self._T_upper_lower * sym_rot_z(phi_3)
                T_b3 = T_b2 * T_23
                T_3E1 = self._T_elbow_endeffector
                T_bE1 = T_b3 * T_3E1

                x_foot = T_bE1 * (Matrix([[0, 0, 0, 1]]).T)
                simple = simplify(x_foot)
                logging.info('Calculating symbolic Jacobi Matrix for Leg %i', i + 1)
                jac = simple.jacobian(self.coords)
                self.J_we_sym.append(jac[:3,:])

            data = {'J_wb': self.J_wb, 'J_we_sym': self.J_we_sym, 'measures': self.measures}
            with open(JACOBIAN_FILE, 'wb') as file:
                logging.info('Saving symbolic Jacobians for further use')
                pickle.dump(data, file, pickle.HIGHEST_PROTOCOL)


    def map_syms(self, values):
        res = {}
        for i, sym in enumerate(self.coords):
            res[sym] = values[i]

        return res

    def update(self, state=INITIAL_COORDS):
        self.T_wb = rot_trans(state[0:3], state[3:6])
        self.T_bf = np.matmul(self._T_base_back_front, self.T_wb)
        self.T_br = np.matmul(self._T_base_back_rear, self.T_wb)

        self.T_base_legs = []
        self.T_legs = []

        for i in range(4):
            [p1, p2, p3] = state[6 + i * 3:9 + i * 3]
            T_t1 = np.matmul(self._T_back_shoulder_up_right if i % 2 == 0 else self._T_back_shoulder_up_left,
                             rot(0, 0, p1))
            T_w1 = np.matmul(self.T_bf if i < 2 else self.T_br, T_t1)
            T_12 = np.matmul(self._T_shoulder_up_shoulder_low, rot(0, 0, p2))
            T_w2 = np.matmul(T_w1, T_12)
            T_23 = np.matmul(self._T_upper_lower, rot(0, 0, p3))
            T_w3 = np.matmul(T_w2, T_23)
            T_3E = self._T_elbow_endeffector
            T_wE = np.matmul(T_w3, T_3E)
            self.T_legs.extend([T_w1, T_12, T_23, T_3E])
            self.T_base_legs.extend([T_w1, T_w2, T_w3, T_wE])
        if self.calc_jac:
            symmap = self.map_syms(state)
            self.J_we = []
            for i in range(4):
                logging.debug(' Evaluating Jacobi Matrix for Leg %i', i + 1)
                self.J_we.append(np.array(self.J_we_sym[i].evalf(subs=symmap)).astype(np.float64))

    def get_base(self):
        return np.array([self.T_wb[:3, 3]]).T

    def plot_dog(self, ax, plot_cyl=False, plot_coord_sys=True, label_sys=True):
        x_base = np.array([[0], [0], [0]])
        x_back_front = transform(x_base, self.T_bf)
        x_back_rear = transform(x_base, self.T_br)

        # plot lines of the back
        plot_line(ax, self.get_base(), x_back_front)
        plot_line(ax, self.get_base(), x_back_rear)
        for i in range(4):
            plot_line(ax, x_back_front if i < 2 else x_back_rear, transform(x_base, self.T_base_legs[i * 4]))

        #plot legs
        for i in range(4):
            self.plot_leg(ax, i, plot_cyl=plot_cyl, plot_coord_sys=plot_coord_sys)

        if label_sys:
            plot_text(ax, self.get_base(), 'base')
            plot_text(ax, x_back_front, 'front')
            plot_text(ax, x_back_rear, 'rear')

        if plot_coord_sys:
            draw_coord_sys(ax, self.T_wb, length=0.5)
            draw_coord_sys(ax, self.T_bf, length=0.5)
            draw_coord_sys(ax, self.T_br, length=0.5)

    def plot_leg(self, ax, leg_no, plot_cyl=False, plot_coord_sys=False, label=True):
        coords = [transform(np.array([[0], [0], [0]]), self.T_base_legs[leg_no * 4 + i]) for i in range(4)]

        if plot_cyl:
            for i in range(3):
                cyl = Cylinder(0.25, 0.5, 10)
                cyl.transform(self.T_base_legs[leg_no * 4 + i])
                cyl.plot(ax)

        # plot bones
        for i in range(3):
            plot_line(ax, coords[i], coords[i + 1])

        if label:
            for i in range(3):
                plot_text(ax, coords[i], leg_no * 3 + 1 + i)

        if plot_coord_sys:
            for i in range(4):
                draw_coord_sys(ax, self.T_base_legs[leg_no * 4 + i], length=0.5)
