import logging

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
from celluloid import Camera
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

from dog import Dog, INITIAL_COORDS
from utils.setaxis_equal import set_axes_equal
from utils.lin_alg import left_damped_pinv, right_damped_pinv, damped_pinv

matplotlib.use("TKAgg")
# uses ffmpeg needs manual installation at the moment

logging.basicConfig(level=logging.INFO)

fig = plt.figure()
camera = Camera(fig)
ax = fig.add_subplot(111, projection="3d")
phis = np.linspace(0, 2 * np.pi, 100)
dog = Dog(calc_jac=True)

USE_CAM = False
TIMESTEP = 1000
TIME_END = 10
PINV_CONST = 0.1
PLOT_INTERVAL = 100


def run():
    state = INITIAL_COORDS
    move_dir = np.array([0, 0, 1, 0, 0, 0])

    for time in range(TIME_END * TIMESTEP+1):
        #state = state + np.matmul(dog.J_wb, move_dir)*(1/timestep)
        print(f'time :{time}/{TIME_END * TIMESTEP}')
        t = time/TIMESTEP
        J_leg = np.vstack(dog.J_we[:3])
        J_leg_inv = damped_pinv(J_leg, PINV_CONST)
        leg_const = np.array([0, 0, 0, 0, 0, 0, 0, 0, 0])#, 0, 0, 0])
        N_leg = np.eye(18)-np.matmul(J_leg_inv, J_leg)
        J_body = dog.J_wb
        J_body_inv = damped_pinv(J_body, PINV_CONST)
        q_leg = np.matmul(J_leg_inv, leg_const)
        q_base = np.matmul(damped_pinv(np.matmul(J_body, N_leg), PINV_CONST), (move_dir-np.matmul(J_body, np.matmul(J_leg_inv, leg_const.T))))
        q_tot = q_leg + np.matmul(N_leg, q_base)

        state += q_tot*(1/TIMESTEP)

        if time % PLOT_INTERVAL == 0:
            ax.cla()
            # Cylinder
            dog.update(state)
            dog.plot_dog(ax, plot_coord_sys=True)
            # Draw parameters
            ax.set_xlabel("X")
            ax.set_ylabel("Y")
            ax.set_zlabel("Z")
            set_axes_equal(ax)
            if USE_CAM:
                camera.snap()
            else:
                plt.pause(0.00005)
            #exit(0)

    if USE_CAM:
        animation = camera.animate()
        animation.save('animation.mp4')
    else:
        plt.show()
