from matplotlib.pyplot import imsave
import numpy as np
from numpy.linalg import inv

def damped_pinv(A, damp):
    [m, n] = np.shape(A)
    if m >= n:
        return left_damped_pinv(A, damp)
    return right_damped_pinv(A, damp)
    
def left_damped_pinv(A, damp):
    return np.matmul(np.linalg.inv(np.matmul(A.T, A)+damp**2*np.eye(np.shape(A)[1])), A.T)
def right_damped_pinv(A, damp):
    return np.matmul(A.T, np.linalg.inv(np.matmul(A, A.T)+damp**2*np.eye(np.shape(A)[0])))