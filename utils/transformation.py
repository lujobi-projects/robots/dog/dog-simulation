import numpy as np
from scipy.spatial.transform import Rotation as R
from sympy import Matrix, cos, sin


def transform(x: np.array, matrix: np.array) -> np.array:
    assert len(x) == 3
    if np.shape(x) == (3,):
        x = np.array([x]).T
    x = np.pad(x, ((0, 1), (0, 0)), mode='constant', constant_values=1)
    res = np.matmul(matrix, x)
    res = res / res[3]
    return res[:][:3]


def translate(x, y, z):
    return np.array([[1, 0, 0, x], [0, 1, 0, y], [0, 0, 1, z], [0, 0, 0, 1]])


def rot(x, y, z):
    mat = R.from_euler('xyz', [x, y, z]).as_matrix()
    mat = np.pad(mat, ((0, 1), (0, 1)), mode='constant', constant_values=0)
    mat[3][3] = 1
    return mat


def rot_trans(x, p):
    mat = R.from_euler('xyz', [p[0], p[1], p[2]]).as_matrix()
    mat = np.pad(mat, ((0, 1), (0, 1)), mode='constant', constant_values=0)
    mat[0][3] = x[0]
    mat[1][3] = x[1]
    mat[2][3] = x[2]
    mat[3][3] = 1
    return mat


def sym_rot_x(angle):
    return Matrix([[1, 0, 0, 0], [0, cos(angle), -sin(angle), 0], [0, sin(angle), cos(angle), 0], [0, 0, 0, 1]])


def sym_rot_y(angle):
    return Matrix([[cos(angle), 0, sin(angle), 0], [0, 1, 0, 0], [-sin(angle), 0, cos(angle), 0], [0, 0, 0, 1]])


def sym_rot_z(angle):
    return Matrix([[cos(angle), -sin(angle), 0, 0], [sin(angle), cos(angle), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
