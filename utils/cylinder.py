import numpy as np

from .transformation import transform


class Cylinder:

    def __init__(self, radius, height, precision=10):
        self.precision = precision
        z = np.linspace(-height / 2, height / 2, precision)
        rad = np.linspace(0, radius, precision)
        theta = np.linspace(0, 2 * np.pi, precision)
        theta_grid, Zc = np.meshgrid(theta, z)
        Xc = radius * np.cos(theta_grid)
        Yc = radius * np.sin(theta_grid)

        self.hull = np.concatenate((Xc.reshape(1, -1), Yc.reshape(1, -1), Zc.reshape(1, -1)))

        _, rad_grid = np.meshgrid(rad, rad)
        x_disc_upper = rad_grid * np.cos(theta_grid)
        x_disc_lower = np.copy(x_disc_upper)
        y_disc_upper = rad_grid * np.sin(theta_grid)
        y_disc_lower = np.copy(y_disc_upper)
        z_disc_lower = np.full_like(y_disc_lower, Zc[0])
        z_disc_upper = np.full_like(y_disc_lower, Zc[-1])

        self.disc_upper = np.concatenate(
            (x_disc_upper.reshape(1, -1), y_disc_upper.reshape(1, -1), z_disc_upper.reshape(1, -1)))
        self.disc_lower = np.concatenate(
            (x_disc_lower.reshape(1, -1), y_disc_lower.reshape(1, -1), z_disc_lower.reshape(1, -1)))

    def transform(self, matrix):
        self.hull = transform(self.hull, matrix)
        self.disc_upper = transform(self.disc_upper, matrix)
        self.disc_lower = transform(self.disc_lower, matrix)

    def plot(self, ax, color='tab:blue', alpha=1.0):
        ax.plot_surface(self.hull[0].reshape(self.precision, -1),
                        self.hull[1].reshape(self.precision, -1),
                        self.hull[2].reshape(self.precision, -1),
                        alpha=alpha,
                        color=color)
        ax.plot_surface(self.disc_upper[0].reshape(self.precision, -1),
                        self.disc_upper[1].reshape(self.precision, -1),
                        self.disc_upper[2].reshape(self.precision, -1),
                        alpha=alpha,
                        color=color)
        ax.plot_surface(self.disc_lower[0].reshape(self.precision, -1),
                        self.disc_lower[1].reshape(self.precision, -1),
                        self.disc_lower[2].reshape(self.precision, -1),
                        alpha=alpha,
                        color=color)
