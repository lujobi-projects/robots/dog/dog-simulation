import numpy as np

from utils.transformation import transform


def plot_line(ax, x1, x2, color='#000000', label='', linewidth=1):
    x1 = x1.T[0]
    x2 = x2.T[0]
    ax.plot([x1[0], x2[0]], [x1[1], x2[1]], [x1[2], x2[2]], '-', linewidth=linewidth, color=color, label=label)


def plot_text(ax, x, label, zdir=None):
    x = x.T[0]
    ax.text(x[0], x[1], x[2], label, zdir=zdir)


def draw_coord_sys(ax, T, length=1, legend=False):
    orig = transform(np.array([0, 0, 0]), T)
    ex = transform(np.array([length, 0, 0]).T, T)
    ey = transform(np.array([0, length, 0]).T, T)
    ez = transform(np.array([0, 0, length]).T, T)

    plot_line(ax, orig, ex, color='tab:green', label='ex' if legend else None, linewidth=3)
    plot_line(ax, orig, ey, color='tab:red', label='ey' if legend else None, linewidth=3)
    plot_line(ax, orig, ez, color='tab:blue', label='ez' if legend else None, linewidth=3)

    if legend:
        ax.legend()
